package com.example.nbp.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

import static com.example.nbp.utils.RestUtils.getCsvResponse;
import static com.example.nbp.utils.RestUtils.getRatesCsvFilename;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Validated
@Slf4j
public class RateRestController {

    private final RateRestService rateRestService;

    @GetMapping("/rates/{code}/years/{startYear}/{endYear}")
    public ResponseEntity<byte[]> getRatesByYears(@PathVariable("code") @NotBlank @Size(min = 3, max = 3) String code,
                                                  @PathVariable("startYear") @Min(2000) @Max(2030) int startYear,
                                                  @PathVariable("endYear") @Min(2000) @Max(2030) int endDYear,
                                                  @RequestParam(value = "days", required = false) List<String> days) {

        String filename = getRatesCsvFilename(code, startYear, endDYear);
        String data = rateRestService.getRatesCSV(code, startYear, endDYear, days);
        return getCsvResponse(filename, data.getBytes());
    }

    @GetMapping("/rates/{code}/dates/{startDate}/{endDate}")
    public ResponseEntity<byte[]> getRatesByDates(@PathVariable("code") @NotBlank @Size(min = 3, max = 3) String code,
                                                  @PathVariable("startDate") @NotBlank @Size(min = 10, max = 10) String startDate,
                                                  @PathVariable("endDate") @NotBlank @Size(min = 10, max = 10) String endDate,
                                                  @RequestParam(value = "days", required = false) List<String> days) {

        String filename = getRatesCsvFilename(code, startDate, endDate);
        String data = rateRestService.getRatesCSV(code, startDate, endDate, days);
        return getCsvResponse(filename, data.getBytes());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> handleException(Exception e) {
        log.warn("Exception: {}", e.getMessage());
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(e.getMessage());
    }
}
