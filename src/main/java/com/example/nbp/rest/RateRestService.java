package com.example.nbp.rest;

import com.example.nbp.client.NbpApiClient;
import com.example.nbp.client.model.ExchangeRatesSeries;
import com.example.nbp.client.model.ExchangeRatesSeries.Rate;
import com.example.nbp.utils.NumberUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.springframework.util.CollectionUtils.isEmpty;

@Service
@RequiredArgsConstructor
@Slf4j
public class RateRestService {

    private static final String FIRST_ONLY = "firstOnly";
    private static final String LAST_ONLY = "lastOnly";

    private final NbpApiClient nbpApiClient;

    public String getRatesCSV(String code, int startYear, int endYear, List<String> days) {

        log.info("[GetRates] code={}, startYear={}, endYear={}, days={}", code, startYear, endYear, days);
        var today = LocalDate.now();

        List<Rate> rates = new ArrayList<>();
        for (int year = startYear; year <= endYear; year++) {
            String startDate = year + "-01-01";
            String endDate = year + "-12-31";
            if (year == today.getYear()) {
                endDate = today.toString();
            } else if (year > today.getYear()) {
                break;
            }
            ExchangeRatesSeries ratesSeries = nbpApiClient.getExchangeRatesSeries(code, startDate, endDate);
            List<Rate> yearRates = filterRates(ratesSeries, days);
            rates.addAll(yearRates);
        }
        return getRatesCSV(rates);
    }

    public String getRatesCSV(String code, String startDate, String endDate, List<String> days) {

        log.info("[GetRates] code={}, startDate={}, endDate={}, days={}", code, startDate, endDate, days);
        ExchangeRatesSeries ratesSeries = nbpApiClient.getExchangeRatesSeries(code, startDate, endDate);
        List<Rate> rates = filterRates(ratesSeries, days);
        return getRatesCSV(rates);
    }

    private List<Rate> filterRates(ExchangeRatesSeries ratesSeries, List<String> days) {
        if (!isEmpty(days)) {
            List<Rate> rates = new ArrayList<>();

            Set<String> daySet = new HashSet<>(days);
            Set<String> yearMonthSet = new HashSet<>();
            boolean firstInMonthOnly = daySet.contains(FIRST_ONLY);

            for (Rate rate : ratesSeries.getRates()) {

                String date = rate.getEffectiveDate();
                String day = date.substring(8); // DD value from YYYY-MM-DD

                if (daySet.contains(day)) {
                    if (firstInMonthOnly) {
                        String yearMonth = date.substring(0, 7); // YYYY-MM value from YYYY-MM-DD
                        if (!yearMonthSet.contains(yearMonth)) {
                            rates.add(rate);
                            yearMonthSet.add(yearMonth);
                        }
                    } else {
                        rates.add(rate);
                    }
                }
            }
            return rates;
        }
        return ratesSeries.getRates();
    }

    private String getRatesCSV(List<Rate> rates) {

        var formatter = NumberUtils.getRateFormatter();
        var builder = new StringBuilder();
        for (Rate rate : rates) {
            builder
                    .append(rate.getEffectiveDate())
                    .append(";")
                    .append(formatter.format(rate.getMid()))
                    .append("\n");
        }
        return builder.toString();
    }
}
