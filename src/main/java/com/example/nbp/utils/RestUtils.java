package com.example.nbp.utils;

import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

public class RestUtils {

    public static String getRatesCsvFilename(String code, int startYear, int endYear) {
        return "NBP_rates_" + code + "_from_" + startYear + "_to_" + endYear + ".csv";
    }

    public static String getRatesCsvFilename(String code, String startDate, String endDate) {
        return "NBP_rates_" + code + "_from_" + startDate + "_to_" + endDate + ".csv";
    }

    public static ResponseEntity<byte[]> getCsvResponse(String filename, byte[] data) {
        String contentDisposition = ContentDisposition.builder("attachment")
                .filename(filename)
                .build().toString();
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_TYPE, "text/csv")
                .header(HttpHeaders.CONTENT_DISPOSITION, contentDisposition)
                .body(data);
    }
}
