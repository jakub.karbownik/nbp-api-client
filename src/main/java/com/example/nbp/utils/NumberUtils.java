package com.example.nbp.utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;

public class NumberUtils {

    public static NumberFormat getRateFormatter() {

        // var formatter = NumberFormat.getInstance(new Locale("pl", "PL"));
        // formatter.setMaximumFractionDigits(4);
        // formatter.setMinimumFractionDigits(4);

        var symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator(',');
        return new DecimalFormat("#.0000", symbols);
    }
}
