package com.example.nbp.client;

import com.example.nbp.client.model.ExchangeRatesSeries;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * NBP API usage examples:
 * https://api.nbp.pl/api/exchangerates/rates/A/CHF
 * https://api.nbp.pl/api/exchangerates/rates/A/CHF/2022-09-30
 * https://api.nbp.pl/api/exchangerates/rates/A/CHF/2022-09-01/2022-09-30
 */
@Component
@RequiredArgsConstructor
@Slf4j
public class NbpApiClient {

    private static final String URL_RATES_FROM_TO = "https://api.nbp.pl/api/exchangerates/rates/A/{code}/{startDate}/{endDate}";

    private final RestTemplate restTemplate = new RestTemplate();

    public ExchangeRatesSeries getExchangeRatesSeries(String code, String startDate, String endDate) {

        log.info("getExchangeRatesSeries: {} {} {}", code, startDate, endDate);

        ResponseEntity<ExchangeRatesSeries> response
                = restTemplate.getForEntity(URL_RATES_FROM_TO, ExchangeRatesSeries.class, code, startDate, endDate);

        HttpStatus httpStatus = response.getStatusCode();
        HttpHeaders httpHeaders = response.getHeaders();

        log.info("NBP response Status: {}", httpStatus);
        log.info("NBP response Content-Type: {}", httpHeaders.getContentType());
        log.info("NBP response Content-Length: {}", httpHeaders.getContentLength());
        log.info("NBP response Headers: {}", httpHeaders);

        ExchangeRatesSeries body = response.getBody();
        log.debug("NBP response Body: {}", body);
        return body;
    }
}
