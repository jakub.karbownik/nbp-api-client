package com.example.nbp.client.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * See:
 * https://api.nbp.pl/api/exchangerates/rates/A/EUR
 */
@Data
public class ExchangeRatesSeries {

    private String table;
    private String currency;
    private String code;
    private List<Rate> rates;

    @Data
    public static class Rate {

        private String no;
        private String effectiveDate;
        private BigDecimal mid;
    }
}
