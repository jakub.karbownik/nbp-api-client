package com.example.nbp.utils;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

class NumberUtilsTest {

    @Test
    void testGetRateFormatter() {

        var formatter = NumberUtils.getRateFormatter();

        assertEquals("4,0000", formatter.format(new BigDecimal("4")));
        assertEquals("4,1000", formatter.format(new BigDecimal("4.1")));
        assertEquals("4,1200", formatter.format(new BigDecimal("4.12")));
        assertEquals("4,1230", formatter.format(new BigDecimal("4.123")));
        assertEquals("4,1234", formatter.format(new BigDecimal("4.1234")));
        assertEquals("4,1234", formatter.format(new BigDecimal("4.12345")));
        assertEquals("4,1235", formatter.format(new BigDecimal("4.123456")));
    }
}
