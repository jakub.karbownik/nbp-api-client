# NBP API client

Simple project based on Spring Boot for getting currencies rates from NBP server.

NBP API documentation:

* [PL] https://api.nbp.pl
* [EN] https://api.nbp.pl/en.html

NBP API example:

| NBP API method                                                | Example
| :---                                                          | :---
| /api/exchangerates/tables/{table}                             | https://api.nbp.pl/api/exchangerates/tables/A
| /api/exchangerates/tables/{table}/{date}                      | https://api.nbp.pl/api/exchangerates/tables/A/2022-09-30
| /api/exchangerates/tables/{table}/{startDate}/{endDate}       | https://api.nbp.pl/api/exchangerates/tables/A/2022-09-01/2022-09-30
| /api/exchangerates/rates/{table}/{code}                       | https://api.nbp.pl/api/exchangerates/rates/A/CHF
| /api/exchangerates/rates/{table}/{code}/{date}                | https://api.nbp.pl/api/exchangerates/rates/A/CHF/2022-09-30
| /api/exchangerates/rates/{table}/{code}/{startDate}/{endDate} | https://api.nbp.pl/api/exchangerates/rates/A/CHF/2022-09-01/2022-09-30

Spring Boot project initialization:

* https://start.spring.io

## Running App

```
mvn clean spring-boot:run
```

## App REST API usage

1. Get currency rates by years (from StartYear to EndYEar):

http://localhost:8080/api/rates/CHF/years/2020/2022
http://localhost:8080/api/rates/EUR/years/2020/2022

2. Get currency rates by years with selected days only:

http://localhost:8080/api/rates/CHF/years/2020/2022?days=05,06,07
http://localhost:8080/api/rates/EUR/years/2020/2022?days=05,06,07

3. Get currency rates by years with selected days and with extra filters:

http://localhost:8080/api/rates/CHF/years/2020/2022?days=05,06,07,firstOnly

4. Get currency rates by dates (from StartDate to EndDate):

http://localhost:8080/api/rates/CHF/dates/2022-09-01/2022-09-30
http://localhost:8080/api/rates/EUR/dates/2022-09-01/2022-09-30

5. Get currency rates by dates with selected days only:

http://localhost:8080/api/rates/CHF/dates/2022-09-01/2022-09-30?days=05,06,07
http://localhost:8080/api/rates/EUR/dates/2022-09-01/2022-09-30?days=05,06,07
